import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RedditPage } from './reddit.page';

describe('RedditPage', () => {
  let component: RedditPage;
  let fixture: ComponentFixture<RedditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RedditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
