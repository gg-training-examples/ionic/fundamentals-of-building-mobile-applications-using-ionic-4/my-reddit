import { Component, OnInit } from '@angular/core';
import { RedditService } from '../service/reddit.service';

@Component({
  selector: 'app-reddit',
  templateUrl: './reddit.page.html',
  styleUrls: ['./reddit.page.scss'],
})
export class RedditPage implements OnInit {
  items: any;
  category: string = 'food';
  limit: number = 10;

  constructor(private redditService: RedditService) { }

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts() {
    this.redditService.getPosts(this.category, this.limit)
      .subscribe((reddit) => {
        this.items = reddit.data.children;
      });
  }
}
