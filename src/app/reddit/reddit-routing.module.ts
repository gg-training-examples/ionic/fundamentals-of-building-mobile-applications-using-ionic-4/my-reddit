import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RedditPage } from './reddit.page';

const routes: Routes = [
  {
    path: '',
    component: RedditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedditPageRoutingModule {}
