import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RedditService {
  private baseUrl: string = 'https://www.reddit.com/r';

  constructor(private http: HttpClient) {}

  getPosts(category: string, limit: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${category}/top.json?limit=${limit}`);
  }
}
