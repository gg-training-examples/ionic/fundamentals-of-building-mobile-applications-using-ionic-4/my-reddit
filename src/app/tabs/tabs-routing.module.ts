import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'reddit',
    component: TabsPage,
    children: [
      {
        path: 'reddit',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../reddit/reddit.module').then(m => m.RedditPageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../about/about.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/reddit',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/reddit/reddit',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
